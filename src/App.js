import { useState } from 'react';
import Navbar from './components/Navbar';
import './App.css';

function App() {
  const [counter, setCounter] = useState(0);

  return (
    <div className='App'>
      <Navbar />
      <h1>Contador probando extensiones</h1>
      <button onClick={() => setCounter(counter + 1)}>+</button>
      <p>{counter}</p>
      <button onClick={() => setCounter(counter - 1)}>-</button>
    </div>
  );
}

export default App;
